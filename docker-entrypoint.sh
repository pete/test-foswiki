#!/bin/bash
#
endfile="/var/www/foswiki/working/logs/installation.end"
if [ -f $endfile ] ; then
    rm $endfile
fi
#
# Wait for a while for the symlinks to be created by Kubernetes postStart hook
echo "waiting for postStart hooks to be completed"
sleep 20s



echo "Entry point LocalSite.cfg"
ls -asl /var/www/foswiki/lib/LocalSite.cfg 

echo "...configure Foswki for CERN with OpenID and egroup integration"
 
if [[ ! -e /var/www/foswiki/data/.htpasswd ]]; then
    touch /var/www/foswiki/data/.htpasswd
fi

#
echo "...preparing Solr for Foswiki"

cd /opt/solr/server/solr/configsets
[ -L foswiki_configs ] && rm foswiki_configs
ln -s /var/www/foswiki/solr/configsets/foswiki_configs/ .

cd /opt/solr/server/solr/solr_foswiki
[ -L core.properties ] && rm core.properties
ln -s /var/www/foswiki/solr/cores/foswiki/core.properties


sed -i '/SolrPlugin..Url/s/localhost/solr/' /var/www/foswiki/lib/LocalSite.cfg 


echo "...enabling NatSkin as the default look and feel"
grep -q "Set SKIN = nat" /var/www/foswiki/data/Main/SitePreferences.txt || sed -i '/---++ Appearance/a\ \ \ * Set SKIN = natedit,topicinteraction,solr,pattern' /var/www/foswiki/data/Main/SitePreferences.txt

sed -i '/^.*SKIN.*$/a\ \ \ * Set PATTERNSKIN_NAVIGATION_TOPIC = System/PatternSkinNavigationCern' /var/www/foswiki/data/Main/SitePreferences.txt 
sed -i '/^.*SKIN.*$/a\ \ \ * Set SUBSCRIBEPLUGIN_ACTIVEWEBS = none' /var/www/foswiki/data/Main/SitePreferences.txt 
sed "s/ProjectLogos\/foswiki-logo.png/CERNgraphics\/CERN-Logo-Blue-46.png/g" -i /var/www/foswiki/data/Main/SitePreferences.txt

echo "Update the LocalSite.cfg with Application name = ${APPLICATION_NAME} , Application admin = ${ADMIN_USERNAME}"
sed "s/APPLICATION_NAME/${APPLICATION_NAME}/g" -i  /var/www/foswiki/lib/LocalSite.cfg

#Apply the new password
cd /var/www/foswiki
tools/configure -save -set {Password}=${ADMIN_PASSWORD}

#Use SAML instead of default if we have a certificate installed
samlfile="/var/www/foswiki/saml/cacert.pem"
if [ ! -f $samlfile]
then
      tools/configure -save -set {LoginManager}='Foswiki::LoginManager::SamlLogin';
      tools/configure -save -set {PasswordManager}='none'
else
      echo "use Template Login"
fi

#Give user pete admin rights
sed -i "s|value=\"\"|value=\"PeteJones\"|" /var/www/foswiki/data/Main/AdminGroup.txt
tools/configure -save -set {FeatureAccess}{Configure}="PeteJones"

#Set the Store to RCS Revision
tools/configure -save -set {Store}{Implementation}='Foswiki::Store::RcsLite'

touch /var/www/foswiki/working/logs/installation.end
#
cd /var/www/foswiki
echo "...starting nginx"

nginx -g "daemon off;"
