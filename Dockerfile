FROM alpine:edge

LABEL "maintainer"="peter.l.jones@cern.ch"
LABEL "maintainer"="timlegge"
#
#
ENV PERL_MM_USE_DEFAULT 1

ENV FOSWIKI_LATEST_URL https://github.com/foswiki/distro/releases/download/FoswikiRelease02x01x07/Foswiki-2.1.7.tgz

ENV FOSWIKI_LATEST_SHA1 cacdd16cbb8ce31306aeb8db47ba6d7c792d7132

ENV FOSWIKI_LATEST Foswiki-2.1.7

RUN rm -rf /var/cache/apk/* && \
    rm -rf /tmp/* && \
    sed -n 's/main/testing/p' /etc/apk/repositories >> /etc/apk/repositories && \
    apk update && \
    apk upgrade && \
    apk add ca-certificates imagemagick mailcap musl nginx openssl tzdata bash \
        db htmldoc make gcc db-dev musl-dev \
        grep unzip wget zip perl perl-algorithm-diff perl-algorithm-diff-xs \
        curl enscript lsof markdown nano openldap-clients openssl pandoc rcs rsync texinfo texlive-dvi texmf-dist-latexextra vim zip zlib-dev \
        perl-apache-logformat-compiler perl-archive-zip perl-authen-sasl \
        perl-authcas perl-cache-cache perl-cgi perl-cgi-session \
        perl-class-accessor perl-convert-pem perl-crypt-eksblowfish \
        perl-crypt-jwt perl-crypt-openssl-bignum perl-crypt-openssl-dsa \
        perl-crypt-openssl-random perl-crypt-openssl-rsa \
        perl-crypt-openssl-verify perl-crypt-openssl-x509 perl-crypt-passwdmd5 \
        perl-crypt-random perl-crypt-smime perl-crypt-x509 perl-dancer \
        perl-datetime perl-datetime-format-xsd perl-dbd-mysql perl-dbd-pg \
        perl-dbd-sqlite perl-dbi \
        perl-devel-overloadinfo perl-digest-perl-md5 perl-digest-sha1 \
        perl-email-mime perl-error perl-fcgi perl-fcgi-procmanager \
        perl-file-copy-recursive perl-file-remove perl-file-slurp perl-file-which \
        perl-filesys-notify-simple perl-file-which perl-gd perl-gssapi \
        perl-hash-merge-simple perl-hash-multivalue perl-html-tree \
        perl-image-info perl-io-socket-inet6 perl-json perl-json-xs \
        perl-ldap perl-libwww perl-locale-maketext-lexicon perl-locale-msgfmt \
        perl-lwp-protocol-https perl-mime-base64 perl-module-install \
        perl-module-pluggable perl-moo perl-moose perl-moosex \
        perl-moosex-types perl-moosex-types-common perl-locale-codes \
        perl-moosex-types-datetime perl-moosex-types-uri \
        perl-moox-types-mooselike perl-path-tiny perl-spreadsheet-parseexcel \
        perl-spreadsheet-xlsx perl-stream-buffered perl-sub-exporter-formethods \
        #perl-sereal perl-test-leaktrace perl-text-unidecode perl-text-soundex \
        perl-text-unidecode perl-text-soundex \
        perl-time-parsedate perl-type-tiny perl-uri perl-www-mechanize \
        perl-xml-easy perl-xml-enc perl-xml-generator perl-xml-parser \
        perl-xml-tidy perl-xml-writer perl-xml-xpath perl-yaml perl-yaml-tiny \
        perl-file-mmagic perl-net-saml2 imagemagick-perlmagick graphviz \
        odt2txt antiword lynx poppler-utils perl-email-address-xs perl-chi \
        perl-xml-sig iwatch perl-http-anyua perl-webservice-slack-webapi perl-dev  --update-cache && \
        # perl-libapreq2 -- Apache2::Request - Here for completeness but we use nginx \
    rm -fr /var/cache/apk/APKINDEX.* && \
    perl -MCPAN -e 'install BerkeleyDB,DB_File' && \
    perl -MCPAN -e "CPAN::Shell->notest('install', 'DB_File::Lock')" && \
    apk del make gcc musl-dev perl-dev db-dev 


RUN touch /.bashrc && \
    touch /root/.bashrc  && \
    wget ${FOSWIKI_LATEST_URL} && \
    echo "${FOSWIKI_LATEST_SHA1}  ${FOSWIKI_LATEST}.tgz" > ${FOSWIKI_LATEST}.tgz.sha1 && \
    sha1sum -cs ${FOSWIKI_LATEST}.tgz.sha1 && \
    mkdir -p /var/www && \
    chmod -R 777 /var/www && \
    chmod -R 777 /var/lib  && \
    chmod -R 777 /var/run  && \
    chmod -R 777 /var/log  && \
    mkdir -p /opt/solr && \
    mkdir -p /opt/solr/server && \
    mkdir -p /opt/solr/server/solr && \
    chmod -R 777 /opt/solr && \
    mv ${FOSWIKI_LATEST}.tgz /var/www && \
    cd /var/www && \
    tar xfz ${FOSWIKI_LATEST}.tgz && \
    rm -rf ${FOSWIKI_LATEST}.tgz && \
    mv ${FOSWIKI_LATEST} foswiki 

RUN cd /var/www/foswiki && \
    chmod -R 777 /var/www/foswiki && \
    tools/configure -save -noprompt && \
    tools/configure -save -set {DefaultUrlHost}='https://APPLICATION_NAME.web.cern.ch' && \
    tools/configure -save -set {ScriptUrlPath}='/bin' && \
    tools/configure -save -set {ScriptUrlPaths}{view}='' && \
    tools/configure -save -set {PubUrlPath}='/pub' && \
    tools/configure -save -set {SafeEnvPath}='/bin:/usr/bin' && \
    tools/configure -save -set {Password}='foswiki'  && \
    tools/configure -save -set {PasswordManager}='none' && \
    tools/configure -save -set {WebMasterEmail}="ADMIN_EMAIL"  && \
    tools/configure -save -set {EnableEmail}=1  && \
#    tools/configure -save -set {Email}{MailMethod}='MailProgram'  && \
    tools/configure -save -set {MailProgram}='/usr/sbin/sendmail -t -oi -oeq'  && \
    tools/configure -save -set {AntiSpam}{EmailPadding}='NOSPAM'  && \
    tools/extension_installer SolrPlugin -r -enable install && \
    tools/extension_installer ActionTrackerPlugin -r -enable install && \
    tools/extension_installer AttachContentPlugin -r -enable install && \
    tools/extension_installer AutoRedirectPlugin -r -enable install && \
    tools/extension_installer AutoTemplatePlugin -r -enable install && \
    tools/extension_installer BreadCrumbsPlugin -r -enable install && \
    tools/extension_installer NatSkin -r -enable install && \
    tools/extension_installer JQPhotoSwipeContrib -r -enable install && \
    tools/extension_installer CaptchaPlugin -r -enable install && \
    tools/extension_installer ChartPlugin -r -enable install && \
    tools/extension_installer ClassificationPlugin -r -enable install && \
    tools/extension_installer CopyContrib -r -enable install && \
    tools/extension_installer DBCacheContrib -r -enable install && \
    tools/extension_installer DBCachePlugin -r -enable install && \
    tools/extension_installer DiffPlugin -r -enable install && \
    tools/extension_installer DigestPlugin -r -enable install && \
    tools/extension_installer DocumentViewerPlugin -r -enable install && \
    tools/extension_installer EditChapterPlugin -r -enable install && \
    tools/extension_installer FarscrollContrib -r -enable install && \
    tools/extension_installer FlexFormPlugin -r -enable install && \
    tools/extension_installer FlexWebListPlugin -r -enable install && \
    tools/extension_installer FilterPlugin -r -enable install && \
    tools/extension_installer GraphvizPlugin -r -enable install && \
    tools/extension_installer GenPDFAddOn -r -enable install && \
    tools/extension_installer GridLayoutPlugin -r -enable install && \
    tools/extension_installer ImageGalleryPlugin -r -enable install && \
    tools/extension_installer ImagePlugin -r -enable install && \
    tools/extension_installer InfiniteScrollContrib -r -enable install && \
    tools/extension_installer JQAutoColorContrib -r -enable install && \
    tools/extension_installer JQDataTablesPlugin -r -enable install && \
    tools/extension_installer JQFullCalendarPlugin  -r -enable install && \
    tools/extension_installer JQGridPlugin -r -enable install && \
    tools/extension_installer JQMomentContrib -r -enable install && \
    tools/extension_installer JQSelect2Contrib -r -enable install && \
    tools/extension_installer JQSerialPagerContrib -r -enable install && \
    tools/extension_installer JQTwistyContrib -r -enable install && \
    tools/extension_installer JSTreeContrib -r -enable install && \
    tools/extension_installer LatexModePlugin -r -enable install && \
    tools/extension_installer LdapContrib -r install && \
    tools/extension_installer LdapNgPlugin -r install && \
    tools/extension_installer LikePlugin -r -enable install && \
    tools/extension_installer ListyPlugin -r -enable install && \
    tools/extension_installer LoremIpsumPlugin  -r -enable install && \
    tools/extension_installer MathModePlugin -r -enable install  && \
    tools/extension_installer MarkdownPlugin -r -enable install  && \
    tools/extension_installer MediaElementPlugin -r -enable install && \
    tools/extension_installer NatSkinPlugin -r -enable install && \
    tools/extension_installer MetaCommentPlugin -r -enable install && \
    tools/extension_installer MetaDataPlugin -r -enable install && \
    tools/extension_installer MimeIconPlugin -r -enable install && \
    tools/extension_installer MoreFormfieldsPlugin -r -enable install && \
    tools/extension_installer MultiLingualPlugin -r -enable install && \
    tools/extension_installer OpenIDLoginContrib -r -enable install && \
    tools/extension_installer PageOptimizerPlugin -r -enable install && \
    tools/extension_installer PubLinkFixupPlugin -r -enable install && \
    tools/extension_installer NewUserPlugin -r -enable install && \
    tools/extension_installer RedDotPlugin -r -enable install && \
    tools/extension_installer RenderPlugin -r -enable install && \
    tools/extension_installer SamlLoginContrib -r install && \
    tools/extension_installer SecurityHeadersPlugin -r -enable install && \
    tools/extension_installer SlideShowPlugin -r -enable install && \
    tools/extension_installer StringifierContrib -r -enable install && \
    tools/extension_installer TagCloudPlugin -r -enable install && \
    tools/extension_installer TopicInteractionPlugin -r -enable install && \
    tools/extension_installer TopicTitlePlugin -r -enable install && \
    tools/extension_installer TreePlugin -r -enable install && \
    tools/extension_installer TreeBrowserPlugin -r -enable install && \
    tools/extension_installer WebLinkPlugin -r -enable install && \
    tools/extension_installer WebFontsContrib -r -enable install && \
    tools/extension_installer WorkflowPlugin -r -enable install && \
    tools/extension_installer XSendFileContrib -r -enable install && \
    tools/configure -save -set {XSendFileContrib}{Header}='X-Accel-Redirect' && \
    tools/configure -save -set {XSendFileContrib}{Location}='/files' && \
    tools/configure -save -set {Plugins}{AutoViewTemplatePlugin}{Enabled}='0' && \
    tools/configure -save -set {Plugins}{LdapNgPlugin}{Enabled}='1' && \
    tools/configure -save -set {Plugins}{SamlLoginContrib}{Enabled}='0' && \
    tools/configure -save -set {Sessions}{ExpireAfter}=-21600 && \
    tools/configure -save -set {Sessions}{UseIPMatching}='0' && \
    tools/configure -save -set {JQDataTablesPlugin}{DefaultConnector} = 'dbcache'  && \
    tools/configure -save -set {JQGridPlugin}{DefaultConnector} = 'dbcache'  && \
    tools/configure -save -set {Cache}{Enabled}='1' && \
    tools/configure -save -set {Extensions}{GenPDFAddOn}{htmldocCmd}='/usr/local/bin/htmldoc'  && \
    tools/configure -save -set {Cache}{Implementation}='Foswiki::PageCache::DBI::SQLite' && \
    tools/configure -save -set {Store}{SearchAlgorithm}='Foswiki::Store::SearchAlgorithms::PurePerl' && \
    tools/configure -save -set {ForceDefaultUrlHost}=1 && \
    tools/configure -save -set {INCLUDE}{AllowURLs}=1 && \
    tools/configure -save -set {AllowRedirectUrl}=0 && \
    tools/configure -save -set {Trace}{LoginManager}=0 && \
    tools/configure -save -set {Register}{AllowLoginName}=1 && \
    tools/configure -save -set {Register}{EnableNewUserRegistration}=0 && \
    tools/configure -save -set {NewUserPlugin}{NewUserTemplate}='%SYSTEMWEB%.NewUserTemplate' && \
    tools/configure -save -set {UserInterfaceInternationalisation}=1 && \
    tools/configure -save -set {Stats}{AutoCreateTopic}='Always' && \
    tools/configure -save -set {Extensions}{PlainFileStoreContrib}{CheckForRCS}=0 && \
    tools/configure -save -set {Plugins}{AutoTemplatePlugin}{ViewTemplateRules}="{ 'ChangeEmailAddress' => 'ChangeEmailAddressView',  'ChangePassword' => 'ChangePasswordView', 'ResetPassword' => 'ResetPasswordView', 'SiteChanges' => 'SiteChangesView',  'UserRegistration' => 'UserRegistrationView', 'WebAtom' => 'WebAtomView', 'WebChanges' => 'WebChangesView',  'WebCreateNewTopic' => 'WebCreateNewTopicView',  'WebIndex' => 'WebIndexView', 'WebRss' => 'WebRssView',  'WebSearch' => 'SolrSearchView',  'WebSearchAdvanced' => 'WebSearchAdvancedView', 'WebTopicList' => 'WebTopicListView', 'WikiGroups' => 'WikiGroupsView',  'WikiUsers' => 'WikiUsersView'}" && \
    tools/configure -save -set {JQueryPlugin}{Plugins}{Placeholder}{Enabled}=0 && \
    tools/configure -save -set {Languages}{fr}{Enabled}=1 && \
    tools/configure -save -set {AuthScripts}='attach,compareauth,configure,edit,manage,previewauth,diffauth,rdiffauth,rename,restauth,save,statistics,upload,viewauth,viewfileauth' && \
    tools/configure -save -set {Ldap}{AllowChangePassword}=0 && \
    tools/configure -save -set {Ldap}{Base}='DC=cern,DC=ch' && \
    tools/configure -save -set {Ldap}{BindDN}='' && \
    tools/configure -save -set {Ldap}{BindPassword}='secret' && \
    tools/configure -save -set {Ldap}{CaseSensitiveLogin}=0 && \
    tools/configure -save -set {Ldap}{CharSet}='' && \
    tools/configure -save -set {Ldap}{Debug}=0 && \
    tools/configure -save -set {Ldap}{DefaultCacheExpire}='0' && \
    tools/configure -save -set {Ldap}{Exclude}='WikiGuest, ProjectContributor, RegistrationAgent, UnknownUser, AdminGroup, NobodyGroup, AdminUser, admin, guest' && \
    tools/configure -save -set {Ldap}{GroupAttribute}='cn' && \
    tools/configure -save -set  {Ldap}{GroupBase}="[  'OU=e-groups,OU=Workgroups,DC=cern,DC=ch' ]" && \
    tools/configure -save -set  {Ldap}{GroupFilter}='objectClass=group' && \
    tools/configure -save -set {Ldap}{GroupScope}='sub' && \
    tools/configure -save -set {Ldap}{Host}='xldap.cern.ch' && \
    tools/configure -save -set {Ldap}{IPv6}=0 && \
    tools/configure -save -set {Ldap}{IgnorePrivateGroups}=1 && \
    tools/configure -save -set {Ldap}{IgnoreReferrals}=0 && \
    tools/configure -save -set {Ldap}{IgnoreViewRightsInSearch}=0 && \
    tools/configure -save -set {Ldap}{IndexEmails}=1 && \
    tools/configure -save -set {Ldap}{InnerGroupAttribute}='member' && \
    tools/configure -save -set {Ldap}{KerberosKeyTab}='/etc/krb5.keytab' && \
    tools/configure -save -set {Ldap}{LoginAttribute}='cn' && \
    tools/configure -save -set {Ldap}{LoginFilter}='objectClass=organizationalPerson' && \
    tools/configure -save -set {Ldap}{MailAttribute}='mail' && \
    tools/configure -save -set {Ldap}{MapGroups}=1 && \
    tools/configure -save -set {Ldap}{MaxCacheAge}='0' && \
    tools/configure -save -set {Ldap}{MemberAttribute}='member' && \
    tools/configure -save -set {Ldap}{MemberIndirection}=1 && \
    tools/configure -save -set {Ldap}{MergeGroups}=1 && \
    tools/configure -save -set {Ldap}{NormalizeGroupNames}=0 && \
    tools/configure -save -set {Ldap}{NormalizeLoginNames}=0 && \
    tools/configure -save -set {Ldap}{NormalizeWikiNames}=1 && \
    tools/configure -save  -set {Ldap}{PageSize}='500' && \
    tools/configure -save -set {Ldap}{PersonAttribures}="{  'c' => 'Country',  'company' => 'OrganisationName',  'department' => 'Department',  'division' => 'Division',  'facsimileTelephoneNumber' => 'Telefax',  'givenName' => 'FirstName',  'l' => 'Location',  'mail' => 'Email',  'manager' => 'Manager',  'mobile' => 'Mobile',  'physicalDeliveryOfficeName' => 'Address',  'postalAddress' => 'Address',  'sAMAccountName' => 'LoginName','sn' => 'LastName',  'streetAddress' => 'Address',  'telephoneNumber' => 'Telephone',  'title' => 'Title',  'uid' => 'LoginName'}" && \
    tools/configure -save -set {Ldap}{PersonDataForm}='UserForm' && \
    tools/configure -save -set {Ldap}{Port}='389' && \
    tools/configure -save -set {Ldap}{Precache}=1 && \
    tools/configure -save -set {Ldap}{PreferLocalSettings}=1 && \
    tools/configure -save -set {Ldap}{PrimaryGroupAttribute}='' && \
    tools/configure -save -set {Ldap}{RewriteGroups}={} && \
    tools/configure -save -set {Ldap}{RewriteLoginNames}={} && \
    tools/configure -save -set {Ldap}{RewriteWikiNames}={ '^(.*)@.*$' => '\$1' } && \
    tools/configure -save -set {Ldap}{SASLMechanism}='PLAIN CRAM-MD5 EXTERNAL ANONYMOUS' && \
    tools/configure -save -set {Ldap}{SecondaryPasswordManager}='none' && \
    tools/configure -save -set {Ldap}{TLSCAFile}='' && \
    tools/configure -save -set {Ldap}{TLSCAPath}='' && \
    tools/configure -save -set {Ldap}{TLSClientCert}='' && \
    tools/configure -save -set {Ldap}{TLSClientKey}='' && \
    tools/configure -save -set {Ldap}{TLSSSLVersion}='tlsv1' && \
    tools/configure -save -set {Ldap}{TLSVerify}='required' && \
    tools/configure -save -set {Ldap}{Timeout}='5' && \
    tools/configure -save -set {Ldap}{UseCanonicalUserIDs}=0 && \
    tools/configure -save -set {Ldap}{UseSASL}=0 && \
    tools/configure -save -set {Ldap}{UseTLS}=0 && \
    tools/configure -save -set {Ldap}{UserBase}="[ 'OU=Users,OU=Organic Units,DC=cern,DC=ch',  'OU=Externals,DC=cern,DC=ch']" && \
    tools/configure -save -set {Ldap}{UserMappingTopic}=''  && \
    tools/configure -save -set {Ldap}{UserScope}='sub' && \
    tools/configure -save -set {Ldap}{Version}='3' && \
    tools/configure -save -set {Ldap}{WikiGroupsBackoff}=1 && \
    tools/configure -save -set {Ldap}{WikiNameAliases}='' && \
    tools/configure -save -set {Ldap}{WikiNameAttributes}='displayName, employeeType' && \
    tools/configure -save -set {SolrPlugin}{EnableOnSaveUpdates}=1  && \
    tools/configure -save -set {SolrPlugin}{EnableOnUploadUpdates}=1  && \
    tools/configure -save -set {SolrPlugin}{EnableOnRenameUpdates}=1  && \
#
    tools/configure -save -set {Email}{MailMethod}='Net::SMTP (STARTTLS)'  && \
    tools/configure -save -set {Saml}{AttributeMap}="{  'CernMail' => 'CernMailUpn',  'DisplayName' => 'http://schemas.xmlsoap.org/claims/DisplayName', 'Email' => 'http://schemas.xmlsoap.org/claims/EmailAddress','FirstName' => 'http://schemas.xmlsoap.org/claims/Firstname', 'HomeInstitute' => 'http://schemas.xmlsoap.org/claims/HomeInstitute',  'Language' => 'http://schemas.xmlsoap.org/claims/PreferredLanguage', 'LastName' => 'http://schemas.xmlsoap.org/claims/Lastname' }"  && \                                                                    
    tools/configure -save -set {Saml}{Debug}=1  && \                                                                                                 
    tools/configure -save -set {Saml}{EmailAttributes}='http://schemas.xmlsoap.org/claims/EmailAddress' && \
    tools/configure -save -set {Saml}{ForbiddenWikinames}='AdminUser,ProjectContributor,RegistrationAgent' && \
    tools/configure -save -set {Saml}{SupportSLO}=1  && \                                                      
    tools/configure -save -set {Saml}{UserFormMatch}=0   && \                                                 
    tools/configure -save -set {Saml}{UserFormMatchField}='Email'   && \                                      
    tools/configure -save -set {Saml}{WikiNameAttributes}='http://schemas.xmlsoap.org/claims/Firstname,http://schemas.xmlsoap.org/claims/Lastname' && \
    tools/configure -save -set {Saml}{acs_url_artifact}=''  && \                                                                                       
    tools/configure -save -set {Saml}{acs_url_post}='/bin/login?saml=acs'  && \                                                                        
    tools/configure -save -set {Saml}{cacert}='/var/www/foswiki/saml/cacert.pem'   && \                                                                
    tools/configure -save -set {Saml}{error_url}='/bin/login?saml=error'   && \                                                                        
    tools/configure -save -set {Saml}{issuer}='https://APPLICATION_NAME.web.cern.ch/'    && \                                                               
    tools/configure -save -set {Saml}{metadata}='https://auth.cern.ch/auth/realms/cern/protocol/saml/descriptor'    && \                               
    tools/configure -save -set {Saml}{org_contact}='peter.l.jones@cern.ch'  && \                                                                       
    tools/configure -save -set {Saml}{org_display_name}='Foswiki CERN'  && \                                                                           
    tools/configure -save -set {Saml}{org_name}='Foswiki'  && \                                                                                        
    tools/configure -save -set {Saml}{provider_name}='Foswiki'  && \                                                                                   
    tools/configure -save -set {Saml}{sign_metatdata}=1   && \                                                                                         
    tools/configure -save -set {Saml}{slo_url_post}='/bin/login?saml=slo_post'  && \                                                                   
    tools/configure -save -set {Saml}{slo_url_redirect}='/bin/login?saml=slo_redirect'  && \                                                           
    tools/configure -save -set {Saml}{sls_double_encoded_response}=0 && \
    tools/configure -save -set {Saml}{sls_force_lcase_url_encoding}=0 && \
    tools/configure -save -set {Saml}{sp_signing_cert}='/var/www/foswiki/saml/sign.pem' && \
    tools/configure -save -set {Saml}{sp_signing_key}='/var/www/foswiki/saml/sign.key' && \
    tools/configure -save -set {Saml}{url}='https://APPLICATION_NAME.web.cern.ch/' && \
#
    tools/configure -save -set {SMTP}{Debug}=1  && \
    tools/configure -save -set {SMTP}{DebugFlags}=''  && \
    tools/configure -save -set {SMTP}{MAILHOST}='cernmx.cern.ch:25'  && \
    tools/configure -save -set {SMTP}{Password}=''  && \
    tools/configure -save -set {SMTP}{SENDERHOST}='APPLICATION_NAME'  && \
    tools/configure -save -set {SMTP}{Username}=''  && \
#
    tools/configure -save -set {Extensions}{OpenID}{Default}{ClientID}='OIDC_CLIENT_ID'  && \
    tools/configure -save -set {Extensions}{OpenID}{Default}{ClientSecret}='OIDC_CLIENT_SECRET' && \
    tools/configure -save -set {Extensions}{OpenID}{Default}{DiscoveryURL}='https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration' && \
    tools/configure -save -set {Extensions}{OpenID}{Default}{RedirectURL}='https://APPLICATION_NAME.web.cern.ch/bin/login' && \
#
    tools/configure -save -set {LoginManager}='Foswiki::LoginManager::TemplateLogin' && \
    tools/configure -save -set {TopicUserMapping}{ForceManageEmails}=1 && \
    chmod -R 777 /var/www/foswiki && \
    echo "LocalSite.cfg" && \
    ls -asl /var/www/foswiki/lib/LocalSite.cfg && \
    rm -fr /var/www/foswiki/working/configure/download/* && \
    rm -fr /var/www/foswiki/working/configure/backup/* && \
    mkdir /var/www/foswiki/pub/System/CERNgraphics/


RUN mkdir -p /run/nginx && \
    chmod -R 777 /run/nginx && \
    chmod -R 777 /etc/crontabs && \
    mkdir -p /etc/nginx/conf.d

#Copy over CERN spcific data
COPY cern/foswiki /var/www/foswiki

RUN chmod -R 777  /var/www/foswiki/

#ADD cern/cern_configuration.tar.gz /tmp/

COPY nginx.default.conf /etc/nginx/http.d/default.conf
COPY nginx.default.conf /etc/nginx/conf.d/default.conf
COPY docker-entrypoint.sh docker-entrypoint.sh


EXPOSE 8080

CMD ["sh", "docker-entrypoint.sh"]
